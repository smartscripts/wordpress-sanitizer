WordPress-Sanitizer
===================

Optimizer WordPress HTML and make WordPress pages load faster

// ============================== DISCLAIMER ================================

/*
 * De scripts op deze pagina bevinden zich nog in bèta-stadium. Gebruik ervan is geheel voor eigen risico. SmartScripts is niet verantwoordelijk voor eventuele schade ontstaan door gebruik van deze scripts.
 *
 */



// ============================== INFORMATIE ==============================

/**
 *
 * ========================== FEATURES VAN WORDPRESS SANITIZER ============
 *
 * Verwijdert overbodige javascripts en stylesheets uit de pagina's.
 *
 * Kort de lange, absolute url's van WordPress in tot verkorte versies en herschrijft die naar aliassen, zodat een site minder herkenbaar wordt als een WordPress-site (verdediging tegen hackers).
 *
 * Combineert alle overgebleven resources (javascripts en stylesheets), zowel extern als embedded geladen) tot één resource voor resp. javascript en CSS. Hierdoor laden pagina's sneller, omdat ze minder requests naar de server hoeven te doen.
 *
 * Met Wordpress Sanitizer valt veel snelheidswinst bij het laden van pagina's te behalen. Alle HTML, CSS en javascript wordt namelijk in (met gzip gecomprimeerde) servercachebestanden opgeslagen. Bij volgende verzoeken worden deze cachebestanden:
 * 1) ofwel rechtsstreeks naar nieuwe bezoekers gestuurd (soms zelfs onder tussenkomst van PHP, indien je rewritemaps kunt instellen op je server),
 * 2) ofwel dankzij de cachecontrol van van WordPress Sanitizer helemaal niet meer verstuurd en direct uit de browsercache geladen (not modified headers).
 *
 * Er is een voorziening voor het in één keer wissen van alle servercache.
 *
 * De HTML van de pagina's wordt netjes geformateerd en eventuele fouten in de HTML voor zover mogelijk gerepareerd.
 *
 * Kan de actieve pagina markeren in het sitemenu.
 *
 *
 *
 *
 * ============================== HOWTOS =================================
 *
 * De optimalisaties door onderstaande classes worden geactiveerd op de volgende manier:
 *
 * 1) WordPressSanitizer wordt geladen bovenaan in functions.php:
	define("WPTHEMEFOLDER", dirname(__FILE__));
	require_once(WPTHEMEFOLDER . "/wordpress-sanitizer.php");
 *
 * 2) bovenaan in index.php (of een vergelijkbare pagina, bijvoorbeeld page.php) in de themamap wordt servercaching gestart met wordpressSanitizer::init_caching(new wordpressSanitizerConfigurator());
 *
 * @see wordpressSanitizer::init_caching()
 *
 * 3) in datzelfde bestand in de themamap wordt de HTML afgevangen en geoptimaliseerd via wordpressSanitizer::correct_and_echo():
 *
 * @see wordpressSanitizer::sanitize_and_echo()
 *
 * 4) De instellingen voor WordPressSanitizer dien je te doen in wordpress-sanitizer-config.php:
 *
 * @see wordpressSanitizerConfiguration::get_options()
 *
 *
 *
 * Actiefmarkering pagina's in menu of van link naar homepagina (deze code kan bijvoorbeeld in header.php in de themamap staan):
 *
 * 1) actiefmarkering in menu (cijfers zijn id's van posts in database):

<?php ob_start(); ?>

<li><a href="326" class="hemelsblauw">bestellen</a></li>
<li><a href="86" class="verkeersoranje">etalage</a></li>
<li><a href="352" class="signaalgroen">over ons</a></li>
<li><a href="358" class="heidepaars">contact</a></li>

<?php wordpressSanitizer::mark_active_page(ob_get_clean()); ?>


 * 2) actiefmarkering (lees: verwijdering) van de link naar de homepagina op die homepagina zelf:
<?php
//start het caching proces:
require_once("wordpress-sanitizer.php");
ob_start();
?>

<a href="45"><img src="/wp-content/themes/coloursole/images/banner-coloursoles.gif" alt=""></a>

<?php
//argument true betekent: dit "menu" heeft geen list-items:
wordpressSanitizer::mark_active_page(ob_get_clean(), true);
?>


 * ================================= VEREISTEN =========================
 *
 * 1) In de rootmap van de site dienen de SCHRIJFBARE mappen "cache", "css", "images" en "scripts" aanwezig te zijn
 *
 * 2) Kopieer ook de map /images naar je server.
 *
 * 3) Je server dient mod_rewrite te ondersteunen



 * =================== OPTIONELE UITBREIDING MET REWRITEMAPS ===========
 *
 * Als je server rewritemaps ondersteunt en je admin-rechten hebt op die server, kun je gebruik maken van rewritemaps. Daarmee kan de door WordPressSanitizer opgeslagen servercache rechtstreeks vanuit Apache geëchood worden, wat vele malen sneller gaat dan echoën van die cache met behulp van PHP.
 *
 * Je dient de voor WordPressSanitizer vereiste rewritemaps als volgt in te stellen (heb je geen admin-rechten, dan kun je ook geen rewritemaps instellen):
 *
 * 1) plaats noslashes.pl (te vinden in de map /rewritemap in het zip-bestand) ergens op je server (bij voorkeur buiten de rootmap van je site) en maak het uitvoerbaar.
 *
 * 2) plaats in vhost.conf (buiten een dir-container!) deze code:

RewriteMap noslashes "prg:/usr/bin/perl /pad/naar/noslashes.pl"

 * Uiteraard zul je nog even moeten controleren of het hier opgegeven pad naar perl (ook in de eerste regel van noslashes.pl) klopt voor jouw server.
 *
 * 3) controleer dat je de schrijfbare mappen "cache" en "javascript" in de root van je domein hebt geplaatst
 *
 * 4) herstart Apache, bijvoorbeeld met "service httpd restart" of met "sudo /etc/init.d/apache2 restart" onder Ubuntu (ik heb niet gecontroleerd of rewritemaps te gebruiken zijn in de Ubuntu-image).
 *
 * 5) de rewritemap wordt aangeroepen in .htaccess. Kun je geen rewritemaps instellen, dan kun je het blok met de rewriterule die de rewritemap "noslashes" aanroept met een gerust hart verwijderen (zie toelichting in .htaccess).
 *
 * Ook zonder rewritemaps zal WordPressSanitizer nog steeds goed werken, alleen net iets minder snel.
 *
 */


