<?php

require_once(dirname(__FILE__) . "/wordpress-sanitizer.php");
wordpressSanitizer::init_caching(new wordpressSanitizerConfiguration());

// één extra ob_start nodig (in init_caching() ook al ob_start() aangeroepen), om later problemen met de outputbuffer te voorkomen.
ob_start();

get_header(); ?>
<section role="main">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>
</section>
</div>

<?php

get_footer();

//laad de HTML-formatter:
require_once(dirname(__FILE__) . "/wordpress-sanitizer-htmllawed.php");

//kort onder andere de absolute paden van WordPress in en maak de site minder herkenbaar als een WordPress-site:
wordpressSanitizer::sanitize_and_echo();

