<?php
// ================== CONFIGURATIE VAN WORDPRESS SANITIZER HIER ==============



interface iConfigurator {

	function get_options ();
}


/** Via deze class worden de opties van wordpressSanitizer ingesteld (dependency injection pattern)
 *
 * Class wordpressSanitizerConfigurator
 *
 * Hier aangeroepen:
 * @see wordpressSanitizer::init_caching()
 *
 * @package WordPressSanitizer
 */
class wordpressSanitizerConfiguration implements iConfigurator {

	/** Deze methode - aangeroepen in wordpressSanitizer::init_caching() - levert een array aan met instellingen voor wordpressUtilties. Voor systematisch werken is het het beste als die opties enkel via onderstaande methode worden gedaan.
	 *
	 * @return array */
	public function get_options () {
		return Array(

			/** wil je de caching uitschakelen, zet onderstaande variabele dan op true in plaats van false:
			 * @var bool */
			"disable_cache" => false,

			/** hoeveel dagen blijft de cache geldig:
			 * @var int */
			"days" => 3,

			/** geef hier de naam op van je WordPress-thema:
			 * @var string */
			"theme_name" => "coloursole",

			/**
			 * indien deze true is (ipv false) wordt er een button op de site weergegeven, waarmee je de servercache kunt wissen
			 * @var bool */
			"development_mode" => true,

			/** Indien deze property true is, worden de javascripts op de pagina verwijderd. Is hier echter false ingesteld, dan worden de javascripts ongemoeid gelaten.
			 * @var bool */
			"remove_javascripts" => true,

			/** |-gescheiden lijst van deeltermen uit namen van javascriptbestanden die niet uit de pagina verwijderd mogen worden. De scripts die hier worden opgegeven worden op álle pagina's beschermd.
			 * @var string */
			"protected_resources" => "html5",

			/** Lijst van javascripts die op welke pagina's (|-gescheiden) gehandhaafd moeten worden. Alle andere javascript zijn overbodig (plugins willen nogal eens javascripts plaatsen op pagina's waar die helemaal niet gebruikt worden) en kunnen dus verwijderd worden.
			 *
			 * @var array */
			"javascripts_required_per_page" => Array (

				//format: "deelterm_javascript_bestand" => "deeltermen_paginanamen_die_dit_script_nodig_hebben",

				"jquery" => "contact|test-3",
				"frm_js" => "contact",
				"formidable" => "contact",
				"maps\\.google" => "test-3",
				"wpgm" => "test-3",
			),
		);
	}
}

