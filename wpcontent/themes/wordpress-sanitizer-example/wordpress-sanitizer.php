<?php


// ============================== DISCLAIMER ================================

/*
 * De scripts op deze pagina bevinden zich nog in bèta-stadium. Gebruik ervan is geheel voor eigen risico. SmartScripts is niet verantwoordelijk voor eventuele schade ontstaan door gebruik van deze scripts.
 *
 */



// ============================== INFORMATIE ==============================

/**
 *
 * ========================== FEATURES VAN WORDPRESS SANITIZER ============
 *
 * Verwijdert overbodige javascripts en stylesheets uit de pagina's.
 *
 * Kort de lange, absolute url's van WordPress in tot verkorte versies en herschrijft die naar aliassen, zodat een site minder herkenbaar wordt als een WordPress-site (verdediging tegen hackers).
 *
 * Combineert alle overgebleven resources (javascripts en stylesheets), zowel extern als embedded geladen) tot één resource voor resp. javascript en CSS. Hierdoor laden pagina's sneller, omdat ze minder requests naar de server hoeven te doen.
 *
 * Met Wordpress Sanitizer valt veel snelheidswinst bij het laden van pagina's te behalen. Alle HTML, CSS en javascript wordt namelijk in (met gzip gecomprimeerde) servercachebestanden opgeslagen. Bij volgende verzoeken worden deze cachebestanden:
 * 1) ofwel rechtsstreeks naar nieuwe bezoekers gestuurd (soms zelfs onder tussenkomst van PHP, indien je rewritemaps kunt instellen op je server),
 * 2) ofwel dankzij de cachecontrol van van WordPress Sanitizer helemaal niet meer verstuurd en direct uit de browsercache geladen (not modified headers).
 *
 * Er is een voorziening voor het in één keer wissen van alle servercache.
 *
 * De HTML van de pagina's wordt netjes geformateerd en eventuele fouten in de HTML voor zover mogelijk gerepareerd.
 *
 * Kan de actieve pagina markeren in het sitemenu.
 *
 *
 *
 *
 * ============================== HOWTOS =================================
 *
 * De optimalisaties door onderstaande classes worden geactiveerd op de volgende manier:
 *
 * 1) WordPressSanitizer wordt geladen bovenaan in functions.php:
	define("WPTHEMEFOLDER", dirname(__FILE__));
	require_once(WPTHEMEFOLDER . "/wordpress-sanitizer.php");
 *
 * 2) bovenaan in index.php (of een vergelijkbare pagina, bijvoorbeeld page.php) in de themamap wordt servercaching gestart met wordpressSanitizer::init_caching(new wordpressSanitizerConfigurator());
 *
 * @see wordpressSanitizer::init_caching()
 *
 * 3) in datzelfde bestand in de themamap wordt de HTML afgevangen en geoptimaliseerd via wordpressSanitizer::correct_and_echo():
 *
 * @see wordpressSanitizer::sanitize_and_echo()
 *
 * 4) De instellingen voor WordPressSanitizer dien je te doen in wordpress-sanitizer-config.php:
 *
 * @see wordpressSanitizerConfiguration::get_options()
 *
 *
 *
 * Actiefmarkering pagina's in menu of van link naar homepagina (deze code kan bijvoorbeeld in header.php in de themamap staan):
 *
 * 1) actiefmarkering in menu (cijfers zijn id's van posts in database):

<?php ob_start(); ?>

<li><a href="326" class="hemelsblauw">bestellen</a></li>
<li><a href="86" class="verkeersoranje">etalage</a></li>
<li><a href="352" class="signaalgroen">over ons</a></li>
<li><a href="358" class="heidepaars">contact</a></li>

<?php wordpressSanitizer::mark_active_page(ob_get_clean()); ?>


 * 2) actiefmarkering (lees: verwijdering) van de link naar de homepagina op die homepagina zelf:
<?php
//start het caching proces:
require_once("wordpress-sanitizer.php");
ob_start();
?>

<a href="45"><img src="/wp-content/themes/coloursole/images/banner-coloursoles.gif" alt=""></a>

<?php
//argument true betekent: dit "menu" heeft geen list-items:
wordpressSanitizer::mark_active_page(ob_get_clean(), true);
?>


 * ================================= VEREISTEN =========================
 *
 * 1) In de rootmap van de site dienen de SCHRIJFBARE mappen "cache", "css", "images" en "scripts" aanwezig te zijn
 *
 * 2) Kopieer ook de map /images naar je server.
 *
 * 3) Je server dient mod_rewrite te ondersteunen



 * =================== OPTIONELE UITBREIDING MET REWRITEMAPS ===========
 *
 * Als je server rewritemaps ondersteunt en je admin-rechten hebt op die server, kun je gebruik maken van rewritemaps. Daarmee kan de door WordPressSanitizer opgeslagen servercache rechtstreeks vanuit Apache geëchood worden, wat vele malen sneller gaat dan echoën van die cache met behulp van PHP.
 *
 * Je dient de voor WordPressSanitizer vereiste rewritemaps als volgt in te stellen (heb je geen admin-rechten, dan kun je ook geen rewritemaps instellen):
 *
 * 1) plaats noslashes.pl (te vinden in de map /rewritemap in het zip-bestand) ergens op je server (bij voorkeur buiten de rootmap van je site) en maak het uitvoerbaar.
 *
 * 2) plaats in vhost.conf (buiten een dir-container!) deze code:

RewriteMap noslashes "prg:/usr/bin/perl /pad/naar/noslashes.pl"

 * Uiteraard zul je nog even moeten controleren of het hier opgegeven pad naar perl (ook in de eerste regel van noslashes.pl) klopt voor jouw server.
 *
 * 3) controleer dat je de schrijfbare mappen "cache" en "javascript" in de root van je domein hebt geplaatst
 *
 * 4) herstart Apache, bijvoorbeeld met "service httpd restart" of met "sudo /etc/init.d/apache2 restart" onder Ubuntu (ik heb niet gecontroleerd of rewritemaps te gebruiken zijn in de Ubuntu-image).
 *
 * 5) de rewritemap wordt aangeroepen in .htaccess. Kun je geen rewritemaps instellen, dan kun je het blok met de rewriterule die de rewritemap "noslashes" aanroept met een gerust hart verwijderen (zie toelichting in .htaccess).
 *
 * Ook zonder rewritemaps zal WordPressSanitizer nog steeds goed werken, alleen net iets minder snel.
 *
 */






//=============================== INTERFACES ================================


//interfaces bij onderstaande classes (dat wil zeggen overzichten van hun publieke methoden)

interface iWordpressSanitizerCallbacks {

	static function mark_active_page ($matches);
}

interface iWordpressSanitizer {

	static function init_caching (iConfigurator $configuration);

	static function mark_active_page ($menu, $has_no_listitem = false);

	static function cache_delete();

	static function sanitize_and_echo ();
}


//============================== EINDE INTERFACES ===========================








//============================ LOKAAL OF ONLINE? =============================


//lokaal en online moeten de afbeeldingen op andere manieren verwerkt worden (vanwege verschillende bestandspaden):
if (!defined("locaal")) {
	if (isset($_SERVER, $_SERVER['SERVER_NAME']) && (stristr($_SERVER['SERVER_NAME'], "localhost") || stristr($_SERVER['SERVER_NAME'], "local-"))) {
		define("locaal", true);
	}
	else {
		define("locaal", false);
	}
}



if (!defined("WPTHEMEFOLDER")) {
	define("WPTHEMEFOLDER", dirname(__FILE__));
}




// ======================== DE CLASSES VAN WORDPRESSANITIZER ==================



require_once(WPTHEMEFOLDER . "/wordpress-sanitizer-config.php");

require_once(WPTHEMEFOLDER . "/wordpress-sanitizer-metaboxes.php");





/** Callbacks ten behoeve van WPsanitizer
 *
 * Class wordpressSanitizerCallbacks
 *
 * @package WordPressSanitizer
 */
class wordpressSanitizerCallbacks implements iWordpressSanitizerCallbacks {

	public static function mark_active_page ($matches) {
		$item = $matches[0];

		$has_list_tags = (stristr($item, "<li"));

		preg_match('#href\s*=\s*[\'"](\d+)#', $item, $out);
		$id = $out[1];

		$url = get_page_link($id);

		//verwijder absolute deel uit WordPress-links (anders geen match met $_SERVER['REQUEST_URI'] mogelijk):
		$url = preg_replace('#https?://[^/]+#i', "", $url, 1);

		$mark_active = ($_SERVER['REQUEST_URI'] == $url);

		$linktext = "";
		$success = preg_match('#<a[^>]*>(.*?)</a>#', $item, $out);
		if ($success) {
			$linktext = $out[1];
		}

		$class = "";
		$success = preg_match('#class\s*=\s*[\'"]([^\'"]+)#', $item, $out);
		if ($success) {
			$class = $out[1];
		}

		if ($mark_active) {
			if ($has_list_tags) {
				return "<li class='active {$class}'>{$linktext}</li>";
			}
			else {
				return $linktext;
			}
		}

		else {
			if ($class) {
				$class = " class=\""  . $out[1] . "\"";
			}

			$opener = ($has_list_tags) ? "<li>" : "";
			$closer = ($has_list_tags) ? "</li>" : "";

			return "{$opener}<a " . "href=\"{$url}\"{$class}>{$linktext}</a>{$closer}";
		}
	}
}


/** Voeg cache-control toe aan pagina's en markeer actieve pagina's in menu's
 *
 * Class wpUtilities
 *
 * @see wordpressSanitizer::sanitize_and_echo()
 * @see wordpressSanitizerConfiguration::get_options()
 *
 * @package WordPressSanitizer
 */
class wordpressSanitizer implements iWordpressSanitizer {

	//de instellingen hieronder worden grotendeels via de class wordpressSanitizerConfiguration ingesteld (dependency injection pattern)


	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var bool */
	private static $_disable_cache = false;

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var int */
	private static $_days = 3;

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var string */
	private static $_theme_name = "";

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var bool */
	private static $_development_mode = false;

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var bool */
	private static $_remove_javascripts = true;

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var string */
	private static $_protected_resources = "";

	/** @see wordpressSanitizerConfiguration::get_options()
	 * @var array */
	private static $_javascripts_required_per_page = Array ();




	private static $_initiated = false;

	private static $_post_time = 0;

	/** Combineer alle scripts op de pagina tot één script en sla die op in een extern bestand. Echo tevens de pagina en cache die.
	 *
	 * @see wp_cache
	 */
	public static function sanitize_and_echo () {

		$html = ob_get_clean();

		self::_get_post_time();

		header('Last-Modified: ' . gmdate("D, d M Y H:i:s", self::$_post_time - 2) . " GMT");

		if (self::$_development_mode) {
			$html = str_replace("</body>", "<a href='/wiscache.php' id='wipecache'><img src='/images/wipe.png' height='16' width='16' alt='Verwijder servercache' title='Klik hier om de servercache te wissen'></a>\r\n</body>", $html);
		}

		$html = str_replace('wp-login.php', "inloggenwp.php", $html);

		//pas de regex voor te verwijderen javascripts afhankelijk van de pagina aan (alleen op pagina's die een bepaald javascript vereisen wordt dat javascript gehandhaafd):
		foreach (self::$_javascripts_required_per_page as $script => $pages) {
			if (preg_match('#(?:' . $pages . ')[^/]*/?$#', $_SERVER['REQUEST_URI'])) {
				self::$_protected_resources .= "|" . $script;
			}
		}

		self::_javascripts_combine($html);
		self::_stylesheets_combine($html);

		self::_urls_shorten($html);

		//pleister:
		$html = preg_replace('#/[a-z]/resources#', "/resources", $html);

		//corrigeer/formatteer/beautify de HTML:
		$html = htmlLawed::purify($html);

		//ETag ten behoeve van cache-control zenden:
		header("ETag: " . md5($html));

		echo $html;

		if (defined("WPPOSTCACHEFILE")) {

			file_put_contents(WPPOSTCACHEFILE, $html);
			if (!locaal) {
				chmod(WPPOSTCACHEFILE, 0666);
			}

			$gzipped = gzencode($html);

			file_put_contents(WPPOSTCACHEFILE . ".gzip", $gzipped);
			if (!locaal) {
				chmod(WPPOSTCACHEFILE . ".gzip", 0666);
			}
		}
	}

	private static function _urls_shorten (&$subject) {
		//absolute links inkorten:
		$subject = preg_replace('#https?://[^/><\'") ]+#', "", $subject);

		//obfusceer de standaard WordPress-paden (.htaccess moet hierop zijn afgestemd):
		$subject = str_replace(Array("wp-content/uploads", "/uploads", "wp-content/themes", "wp-content/plugins", "wp-admin", "wp-content", "wp-includes"), Array("resources", "/resources", "t", "p", "a", "c", "i"), $subject);
	}

	private static function _javascripts_combine(&$html) {

		//verwijder alle javascripts op de openbare pagina:
		if (self::$_remove_javascripts) {

			$javascript_construct = "";

			$javascript_already_combined = false;

			$success = preg_match_all('#<script[^>]*?src\s*=\s*[\'"]([^\'"]+)#i', $html, $out);

			$target = $html_target = "";

			$external_sources = ($success) ? $out[1] : Array();

			if (!$success || (count($out[1]) == 1 && stristr($external_sources[0], "html5"))) {
				self::_resources_delete_from_page($html, "javascript", "embedded");
				self::_resources_delete_from_page($html, "javascript", "external");
				return;
			}

			if (!$javascript_already_combined) {
				$all_targets = join("", $external_sources);
				$html_target = "/scripts/" . md5($all_targets) . ".js";
				$target = $_SERVER['DOCUMENT_ROOT'] . $html_target;

				//het cachefile niet opnieuw opslaan als functions.php en de post niet nieuwer zijn dan dat cachefile:
				if (file_exists($target)) {

					$filemtime = filemtime(__FILE__);
					self::_get_post_time();

					$resource_time = filemtime($target);

					if ($resource_time > $filemtime && $resource_time > self::$_post_time) {
						$javascript_already_combined = true;
					}
				}
			}

			if (!$javascript_already_combined && $external_sources) {

				foreach ($external_sources as $src) {
					if (!preg_match('#(?:' . self::$_protected_resources . ')#', $src) || stristr($src, "html5.js")) {
						continue;
					}

					//verwijder eventuele versienummers:
					$file_src = preg_replace('#[?].+$#', "", $_SERVER['DOCUMENT_ROOT'] . $src, 1);

					if (file_exists($file_src)) {
						$javascript_construct .= file_get_contents($file_src) . "\r\n";
					}
				}

				//verwijder alle links naar externe scripts:
				self::_resources_delete_from_page($html, "javascript", "external");


				//lees alle embedded scripts in:
				$success = preg_match_all('#<script(?:[^>](?!src))*>(.*?)</script>#si', $html, $out);

				if ($success) {
					foreach ($out[1] as $script) {

						if (!$script || !preg_match('#(?:' . self::$_protected_resources . ')#', $script)) {
							continue;
						}

						$javascript_construct .= $script . "\r\n";
					}

					//verwijder alle embedded scripts uit de pagina:
					self::_resources_delete_from_page($html, "javascript", "embedded");
				}

				//cache het verzamelde javascript (mits dat er is):
				if ($javascript_construct) {
					file_put_contents($target, $javascript_construct);
					if (!locaal) {
						chmod($target, 0666);
					}

					//sla ook een gzipped versie op:
					$gzipped = gzencode($javascript_construct);
					file_put_contents($target . ".gzip", $gzipped);
					if (!locaal) {
						chmod($target . ".gzip", 0666);
					}
				}
			}

			if (($javascript_already_combined || $javascript_construct) && $html_target) {
				self::_resources_delete_from_page($html, "javascript", "embedded");
				self::_resources_delete_from_page($html, "javascript", "external");

				//link naar het verzamelde javascript:
				$html = str_replace("</body>", "<script src='" . "{$html_target}'></script>\r\n</body>", $html);
			}

		}

	}

	private static function _stylesheets_combine(&$html) {

		//verwijder alle stylesheets op de openbare pagina:
		if (self::$_remove_javascripts) {

			$css_construct = "";

			$sanitizer_css = WPTHEMEFOLDER . "/wordpress-sanitizer.css";
			if (file_exists($sanitizer_css)) {
				$css_construct .= file_get_contents($sanitizer_css) . "\r\n";
			}

			$css_already_combined = false;

			$success = preg_match_all('#<link[^>]*?href\s*=\s*[\'"]([^\'"]+?\.css)#i', $html, $out);

			$target = $html_target = "";
			$external_sources = Array();

			if ($success) {
				$external_sources = $out[1];

				$all_targets = join("", $external_sources);
				$code = md5($all_targets);
				$html_target = "/css/{$code}.css";
				$target = $_SERVER['DOCUMENT_ROOT'] . "/css/{$code}.css";

				//het cachefile niet opnieuw opslaan als functions.php en de post niet nieuwer zijn dan dat cachefile:
				if (file_exists($target)) {

					$filemtime = filemtime(__FILE__);
					self::_get_post_time();

					$resource_time = filemtime($target);

					if ($resource_time > $filemtime && $resource_time > self::$_post_time) {
						$css_already_combined = true;
					}
				}
			}

			if (!$css_already_combined && $external_sources) {

				if ($external_sources) {
					foreach ($external_sources as $src) {
						if (preg_match('#(?:ie\d+\.css$)#', $src)) {
							continue;
						}

						//verwijder eventuele versienummers:
						$file_src = preg_replace('#[?].+$#', "", $_SERVER['DOCUMENT_ROOT'] . $src, 1);

						if (file_exists($file_src)) {
							$css_construct .= file_get_contents($file_src) . "\r\n";
						}
					}
				}

				//verwijder alle links naar externe stylesheets:
				self::_resources_delete_from_page($html, "stylesheet", "external");


				//lees alle embedded CSS in:
				$success = preg_match_all('#<style[^>]*>(.*?)</script>#si', $html, $out);

				if ($success) {
					foreach ($out[1] as $css) {

						if (!$css || !preg_match('#(?:' . self::$_protected_resources . ')#', $css)) {
							continue;
						}

						$css_construct .= $css . "\r\n";
					}

					//verwijder alle embedded stylesheets uit de pagina:
					self::_resources_delete_from_page($html, "stylesheet", "embedded");
				}

				//cache het verzamelde javascript (mits dat er is):
				if ($css_construct) {

					//correcties van de CSS:
					$css_construct = preg_replace('#([\'"])webfonts#', "\\1/t/" . self::$_theme_name . "/webfonts", $css_construct);

					self::_urls_shorten($css_construct);

					file_put_contents($target, $css_construct);
					if (!locaal) {
						chmod($target, 0666);
					}

					//sla ook een gzipped versie op:
					$gzipped = gzencode($css_construct);
					file_put_contents($target . ".gzip", $gzipped);
					if (!locaal) {
						chmod($target . ".gzip", 0666);
					}
				}
			}

			if (($css_already_combined || $css_construct) && $html_target) {
				self::_resources_delete_from_page($html, "stylesheet", "embedded");
				self::_resources_delete_from_page($html, "stylesheet", "external");

				//link naar het verzamelde javascript:
				$html = str_replace("</head>", "<link rel='stylesheet' href='" . "{$html_target}'>\r\n</head>", $html);
			}

		}

	}

	private static function _resources_delete_from_page (&$html, $stream = "javascript", $target = "embedded") {

		if ($stream == "javascript") {
			//verwijder alle embedded scripts uit de pagina:
			if ($target == "embedded") {
				$html = preg_replace('#<script(?:[^>](?!\bsrc))*>.*?</script>#si', "", $html);
			}

			//verwijder alle links naar externe scripts
			else {
				$html = preg_replace('#<script[^>]*?\bsrc\b(?:[^>](?!html5))*?>.*?</script>#si', "", $html);
			}
		}

		//stylesheets:
		else {
			//verwijder alle embedded stylesheets uit de pagina:
			if ($target == "embedded") {
				$html = preg_replace('#<style[^>]*>.*?</style>#si', "", $html);
			}

			//verwijder alle links naar externe stylesheets:
			else {
				$html = preg_replace('#<link(?:[^>](?!ie\d+))*?stylesheet(?:[^>](?!ie\d+))*?>#i', "", $html);
			}
		}
	}

	/**
	 * Start het caching proces; indien er een geldig cachebestand bestaat, wordt dat geëchood, het script afgebroken en WordPress dus verder niet meer geladen.
	 *
	 * @param iConfigurator $configuration Het configuratie-object dat de instellingen aanlevert voor deze class
	 */
	public static function init_caching (iConfigurator $configuration) {

		if (self::$_initiated) {
			return;
		}

		self::$_initiated = true;

		//stel die via het configuratie-object opgehaalde opties in:
		$options = $configuration->get_options();
		foreach ($options as $key => $value) {
			$key = "_" . $key;
			if (property_exists("WordpressSanitizer", $key)) {
				self::$$key = $value;
			}
		}

		$expire = 3600 * 24 * self::$_days;
		header("Cache-Control: max-age={$expire},must-revalidate,proxy-revalidate");


		$success = preg_match('#/(.*?)/?$#', $_SERVER['REQUEST_URI'], $out);

		$url ="unknown";

		if ($success) {
			$url = (!$out[1]) ? "index" : $out[1];

			//verwijder querystrings uit de naam van het cachebestand:
			$url = preg_replace('#[?].+$#', "", $url, 1);

			//pagina's in "submappen" converteren naar een bestandsnaam zonder slashes:
			$url = str_replace("/", "-", $url);
		}

		define("WPPOSTCACHEFILE", $_SERVER['DOCUMENT_ROOT'] . "/cache/{$url}.html");

		//geen caching-technieken toepassen voor een gepost formulier:
		if (!self::$_disable_cache && file_exists(WPPOSTCACHEFILE) && (!isset($_POST) || !$_POST)) {

			$cachetime = filemtime(WPPOSTCACHEFILE);

			$cache_is_valid = (time() - $cachetime <= $expire);

			//cachefile en eventuele not-modified-headers enkel gebruiken wanneer het cachebestand niet ouder is dan het in $expire opgegeven aantal dagen:
			if ($cache_is_valid) {

				//kijken of we alles kunnen afhandelen via een not-modified header:
				$if_modsince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;

				//http_if_none_match contains the etag as it was sent to the browser during a previous request of the same file:
				$if_etag = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) : false;

				$etag = md5_file(WPPOSTCACHEFILE);

				//if the etag is still the same as the etag sent during a previous request or if the file modification time of the online file is less then or equal to the file modifcation time of the file the browser has in its cache, send a not-modified header (in this case you don't have to send the html of the page again):
				if (($if_modsince && $cachetime <= $if_modsince) || $etag == $if_etag) {
					header('HTTP/1.0 304 Not Modified');

					if (ob_get_length()) {
						ob_end_clean();
					}

					//als we hier zijn het script afbreken, we hoeven dan geen content te sturen:
					die;
				}


				//indien ondersteund door de browser van de bezoeker sturen we bij voorkeur het gzipped cachebestand:
				$gzOK = (isset($_SERVER, $_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'));

				if ($gzOK && file_exists(WPPOSTCACHEFILE . ".gzip")) {

					header("Content-Encoding: gzip");
					header("Vary: Accept-Encoding");

					readfile(WPPOSTCACHEFILE . ".gzip");
					die;
				}

				//als we hier zijn, ondersteunt de browser van de bezoeker klaarblijkelijk geen gzip-compressie:
				readfile(WPPOSTCACHEFILE);
				die;
			}
		}

		//vang de output af, om de onzinnige rommel (absolute links) van WordPress te corrigeren en om bestandspaden te obfusceren:
		ob_start();
	}

	/**
	 * @param string $menu
	 * @param bool $has_no_listitem Indien true, dan enkel matchen op links. Bijvoorbeeld handig voor link naar homepagina rondom image.
	 */
	public static function mark_active_page ($menu, $has_no_listitem = false) {

		$regex = ($has_no_listitem) ? '#<a.*?</a>#si' : '#<li.*?</li>#si';

		$menu = preg_replace_callback($regex, "wordpressSanitizerCallbacks::mark_active_page", $menu);

		echo $menu;
	}

	public static function cache_delete() {
		$cachefolders = Array("/cache", "/css", "/scripts");

		$total_count = 0;

		foreach ($cachefolders as $folder) {
			$files = scandir($_SERVER['DOCUMENT_ROOT'] . $folder);
			$count = count($files);

			//lege map met enkel .. en . :
			if ($count == 2) {
				continue;
			}

			$total_count += $count;

			foreach ($files as $file) {

				//scandir() laat pad van files weg:
				$path = $_SERVER['DOCUMENT_ROOT'] . $folder . "/" . $file;

				if (in_array($file, Array(".", "..")) || is_dir($path) || substr($file, 0, 1) == ".") {
					$total_count--;
					continue;
				}

				unlink ($path);

			}
		}

		$link = "<br /><a " . "href=\"/\">naar de homepagina</a>";

		$title_OK = "Cache gewist";
		$message_OK = "{$total_count} bestanden gewist!<br /><strong>Vergeet niet om ook nog je browsercache te wissen...</strong>" . $link;

		$title_error = "Geen cachebestanden gevonden";
		$message_error = "Er waren geen cachebestanden om te wissen..." . $link;

		$success = $total_count;

		self::_message_display($success, $message_OK, $message_error, $title_OK, $title_error);
	}

	/** Geef een melding weer, die afhankelijk van $success ofwel een succesmelding is, ofwel een foutmelding
	 *
	 * @param mixed $success
	 * @param string $message_OK
	 * @param string $message_error
	 * @param string $title_OK
	 * @param string $title_error
	 */
	private static function _message_display($success, $message_OK = "", $message_error = "", $title_OK = "", $title_error = "") {

		$template = WPTHEMEFOLDER . "/wordpress-sanitizer-template.html";

		//indien er geen template bestaat, worden de berichten in een volkomen kale HTML-pagina weergegeven:
		if (!file_exists($template)) {
			if ($success) {
				echo $message_OK;
			}

			else {
				echo $message_error;
			}
		}

		//berichten met gebruik van een template:
		else {
			if ($success) {
				$title = $title_OK;

				$message = $message_OK;
			}

			else {
				$title = $title_error;

				$message = $message_error;
			}

			$template = file_get_contents($template);

			$template = str_replace(Array("((title))", "((message))"), Array($title, $message), $template);

			echo $template;
		}
	}

	/**
	 * Bepaal het tijdstip van publicatie van de post op de huidige pagina
	 */
	private static function _get_post_time() {
		if (!self::$_post_time) {
			ob_start();
			the_date();
			echo " ";
			the_time();
			self::$_post_time = strtotime(ob_end_clean());
		}
	}
}

